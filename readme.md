Scss tasks
Compile scss to css
Minify CSS
Sourcemaps 
Autoprefix
lint  - Need to look at rules (https://stylelint.io/user-guide/rules/list) 

JS tasks
lint
transpile ES6 to ES5

General tasks
browser-sync
Prettier - Need to look at rules (https://prettier.io/docs/en/options.html)

To Do
Compress images with imagemin-cli
Minify JS
Run some default magento tasks e.g npm run magento:setup for bin/magento setup:upgrade
rm -rf /pub/static/*
rm -rf var/di/* var/generation/* var/cache/* var/page_cache/* pub/static/*

Main Tasks
npm run dev: run browser-sync and watch all scss/js files, lint and compile on save
npm run build: build css/js ready for production
npm run clean: clean folders
npm run magento: run some default magento commands

Future:
Add TypeScript and Jest support
Critical JS/CSS